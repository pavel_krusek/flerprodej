//
//  BagdeButton.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 20/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit

class BagdeButton: UIButton {
    
    var lblBadge: UILabel?

    required override init(frame: CGRect) {
        super.init(frame: frame)
        
        lblBadge = UILabel.init(frame: CGRectMake(23, 13, 17, 17))
        lblBadge?.textColor = UIColor(red: 93.0/255.0, green: 176.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        lblBadge?.textAlignment = NSTextAlignment.Center
        //lblBadge?.backgroundColor = UIColor.redColor()
        lblBadge?.font = UIFont.systemFontOfSize(11)
        lblBadge?.text = ""
        
        self.addSubview(lblBadge!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
