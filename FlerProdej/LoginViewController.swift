//
//  LoginViewController.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 20/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit
import SwiftLoader

class LoginViewController: UIViewController {
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = DataManager.manager.userRead() {
            tfName.text = user.username
        }
        

    }

    @IBAction func actionLogin() {
        
        if tfName.text?.characters.count < 1 || tfPassword.text?.characters.count < 1 {
            alert("Musíte vyplnit jméno a heslo")
            
            return
        }
        
        //SwiftLoader.show(animated: true)
        userLogin(tfName.text!, password: tfPassword.text!) { (bool, error) -> () in
        //userLogin("rollingmobile", password: "rmheslo") { (bool, error) -> () in
            //SwiftLoader.hide()
            if bool == true {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                if appDelegate.isSplash == true {
                    appDelegate.isSplash = false
                    appDelegate.dissmissSplashWindow()
                } else {
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
                
            } else {
                self.alert("Se zadanými údaji se nejde zalogovat")
            }
        }

        
    }
    
    @IBAction func actionRegister() {
        
        UIApplication.sharedApplication().openURL(NSURL(string: "http://www.fler.cz/uzivatel/registrace")!)
    }
    
    private func alert(text: String) {
        let alert = UIAlertController(title: "", message:text, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default) { _ in
        }
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }

}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("")
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.actionLogin()
        return false
    }
}
