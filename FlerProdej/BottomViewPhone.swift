//
//  BottomViewPhone.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 25/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit

class BottomViewPhone: UIView {
    
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var switchCallback: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "BottomViewPhone", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        view.frame = bounds
        
        self.addSubview(view);
    }
    
    @IBAction func actionSwitchUser() {
        if let switchCallback = self.switchCallback {
            switchCallback()
        }
    }

}
