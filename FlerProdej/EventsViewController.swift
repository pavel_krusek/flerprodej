//
//  DetailViewController.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 09/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit
import SwiftLoader
import LGSideMenuController
import SwiftyUserDefaults

class EventsViewController: BaseViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var statsScrollView: UIScrollView!
    @IBOutlet weak var pickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblStats: UILabel!
    @IBOutlet weak var ivStatsArraw: UIImageView!
    @IBOutlet weak var pvStats: UIPickerView!
    
    @IBOutlet weak var lblViews: UILabel!
    @IBOutlet weak var lblTurnover: UILabel!
    @IBOutlet weak var lblSell: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var lblFans: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    var btnOrder: BagdeButton!
    var btnMail: BagdeButton!
//    var views: Int?
//    var turnover: String?
//    var sell: String?
//    var likes: Int?
//    var fans: Int?
//    var rating: String?
//    var orders: Int?
//    var mail: Int?
    
    // MARK: - iVars
    
    var selectedStats: StatsTerms = StatsTerms(rawValue: Defaults[.selectedStats])!
    var selectedStatsValue: Int = Defaults[.selectedStats]
    
    private lazy var blurView = UIView.init()
    
    private var btnMenu: UIBarButtonItem?

    private var events = Array<Event>() {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var stats: Statistics? {
        didSet {
            updateStats()
        }
    }
    
    private var eventTypes = Array<EventType>()
    
    private var paging = PagingGenerator<Event>(startOffset: 0, limit: 25)
    
    private var topView = UIView.init(frame: CGRectZero)
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedStats = StatsTerms(rawValue: Defaults[.selectedStats])!
        selectedStatsValue = Defaults[.selectedStats]
        
        //print("sel stats\(selectedStats)")
        lblStats.text = selectedStats.description
        
        btnOrder = BagdeButton.init(frame: CGRectMake(0, 0, 50, 50))
        btnOrder.setImage(UIImage(named: "Order"), forState: .Normal)
        btnOrder.adjustsImageWhenHighlighted = false
        btnOrder.addTarget(self, action: "test", forControlEvents: .TouchUpInside)
        
        btnMail = BagdeButton.init(frame: CGRectMake(0, 0, 50, 50))
        btnMail.setImage(UIImage(named: "Mail"), forState: .Normal)
        btnMail.adjustsImageWhenHighlighted = false
        btnMail.lblBadge!.text = ""
        btnMail.addTarget(self, action: "test", forControlEvents: .TouchUpInside)
        
//        let btnSearch = UIButton.init(frame: CGRectMake(0, 0, 50, 50))
//        btnSearch.setImage(UIImage(named: "Search"), forState: .Normal)
//        btnSearch.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0)
//        btnSearch.adjustsImageWhenHighlighted = false
//        btnSearch.addTarget(self, action: "test", forControlEvents: .TouchUpInside)
        
        let btnItemOrder = UIBarButtonItem.init(customView: btnOrder)
        let btnItemMail = UIBarButtonItem.init(customView: btnMail)
        //let btnItemSearch = UIBarButtonItem.init(customView: btnSearch)
        
        let fixedSpace = UIBarButtonItem.init(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
        fixedSpace.width = 8;
        
        self.navigationItem.rightBarButtonItems = [btnItemMail, fixedSpace, btnItemOrder]
        
        
        
        addBottomView()
        
        tableView.estimatedRowHeight = 150.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0)
        tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 50, 0)
        
        topView.backgroundColor = UIColor(red: 93.0/255.0, green: 176.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        tableView.addSubview(topView)
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            btnMenu = UIBarButtonItem(image: UIImage(named: "Menu"), style: .Plain, target: self.splitViewController!.displayModeButtonItem().target, action: self.splitViewController!.displayModeButtonItem().action)
            self.navigationItem.leftBarButtonItem = btnMenu
        } else {
            btnMenu = UIBarButtonItem(image: UIImage(named: "Menu"), style: .Plain, target: self, action: "actionMenu")
        
            
            let btnLogo = UIBarButtonItem(image: UIImage(named: "Logo"), style: .Plain, target: nil, action: nil)
            self.navigationItem.leftBarButtonItems = [btnMenu!, btnLogo]
//            let ivLogo = UIImageView.init(image: UIImage(named: "Logo"))
//            self.navigationItem.titleView = ivLogo
//            self.navigationItem.leftBarButtonItem = btnMenu
            
//            let titleView = self.navigationItem.titleView
//            if let titleView = titleView {
//                titleView.frame = CGRectMake(50, titleView.frame.origin.y, titleView.frame.size.width, titleView.frame.size.height)
//            }
        }
        
        pickerBottomConstraint.constant = -94
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadData", name: notificationLogin, object: nil)
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: "appEnterForeground", name: UIApplicationWillEnterForegroundNotification, object: nil)

        if User.isLogged() == true {
            loadData()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
//        statsScrollView.contentSize = CGSizeMake(600, 80)
        
//        let titleView = self.navigationItem.titleView
//        if let titleView = titleView {
//            titleView.frame = CGRectMake(50, titleView.frame.origin.y, titleView.frame.size.width, titleView.frame.size.height)
//        }
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            
            UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "deviceDidRotate", name: UIDeviceOrientationDidChangeNotification, object: nil)
            
            deviceDidRotate()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //NSNotificationCenter.defaultCenter().removeObserver(self)
        
        if UIDevice.currentDevice().generatesDeviceOrientationNotifications {
            UIDevice.currentDevice().endGeneratingDeviceOrientationNotifications()
        }
    }
    
    // MARK: - Actions
    
    func actionMenu() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let mainController = appDelegate.window?.rootViewController as! LGSideMenuController
        mainController.showLeftViewAnimated(true, completionHandler: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var topFrame = tableView.bounds;
        topFrame.origin.y = -topFrame.size.height;
        
        topView.frame = topFrame
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
            statsScrollView.contentSize = CGSizeMake(600, 80)
            
//            let titleView = self.navigationItem.titleView
//            if let titleView = titleView {
//                titleView.frame = CGRectMake(50, titleView.frame.origin.y, titleView.frame.size.width, titleView.frame.size.height)
//            }
        }
    }
    
    func test() {

    }

    override func switchUser() {
//        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
//            
//            let alert = UIAlertController(title: "", message:"Přepínání účtů zatím jen na iPadu - na iPhone v příštím buildu (iPhone nepodporuje přímo PopOver, píšu vlastní řešení)", preferredStyle: .Alert)
//            let action = UIAlertAction(title: "OK", style: .Default) { _ in
//            }
//            alert.addAction(action)
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//            return
//        }
        
        let user = DataManager.manager.userRead()
        
        if let user = user {
            let accounts = user.connectedAccounts?.count
            
            if accounts == 0 || accounts == nil {
                let alert = UIAlertController(title: "", message:"Nemáte žádný propojený účet.", preferredStyle: .Alert)
                let action = UIAlertAction(title: "OK", style: .Default) { _ in
                    self.blurView.subviews.forEach({ $0.removeFromSuperview() })
                    self.blurView.removeFromSuperview()
                }
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
                
                return
            }
            
            var vc: UIViewController?
            
            if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
                vc = self.splitViewController
            } else {
//                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                vc = appDelegate.window?.rootViewController as! LGSideMenuController
                
                let nvc = self.slidingPanelController.centerViewController as! UINavigationController
                vc = nvc.topViewController as! EventsViewController
            }
            

            
            UIGraphicsBeginImageContextWithOptions(vc!.view.bounds.size, true, 1)
            vc!.view.drawViewHierarchyInRect(vc!.view.bounds, afterScreenUpdates: true)
            
            let screenshot = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let blur = screenshot.applyBlurWithRadius(3, tintColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.3), saturationDeltaFactor: 1.8, maskImage: nil)
            
            blurView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            blurView.frame = vc!.view.bounds
            blurView.addSubview(UIImageView.init(image: blur))
            blurView.alpha = 0
            
            vc!.view.insertSubview(blurView, atIndex: 100)
            
            UIView.animateWithDuration(0.2, animations: {
                self.blurView.alpha = 1
                }, completion: { (value: Bool) in
                    self.loadUserAccounts(user, accounts: accounts!)
            })
            
        } else {
            let alert = UIAlertController(title: "", message:"Problém s načtením účtů", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default) { _ in
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }

    }
    
    private func loadUserAccounts(user: User, accounts: Int) {
            
        var height = accounts
        
        if accounts > 5 {
            height = 5
        }
        
        var svc: UIViewController?
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            svc = self.splitViewController
        } else {
            svc = self
//            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            svc = appDelegate.window?.rootViewController as! LGSideMenuController
        }

        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier(String.className(SwitchUserViewController)) as! SwitchUserViewController
        vc.accounts = user.connectedAccounts!
        vc.dismissCallback = { [weak self] in
            self!.blurView.subviews.forEach({ $0.removeFromSuperview() })
            self!.blurView.removeFromSuperview()
        }
        
        vc.modalPresentationStyle = .Popover
        vc.preferredContentSize = CGSizeMake(335, 89+60+17+CGFloat(74*height))
        
        let popoverMenuViewController = vc.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = svc!.view
        popoverMenuViewController?.sourceRect = self.sourceRectForCenteredPopoverController()
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    func sourceRectForCenteredPopoverController() -> CGRect {
        
        var vc: UIViewController?
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            vc = self.splitViewController
        } else {
            vc = self
//            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            vc = appDelegate.window?.rootViewController as! LGSideMenuController
        }
        
        let r:CGRect = CGRectMake((vc!.view.bounds.width) * 0.5, (vc!.view.bounds.height) * 0.5, 1, 1);
        
        return r
    }
    
    @IBAction func actionSortStatistics() {
        
        var rotate: CGFloat = 0.0
        
        if pickerBottomConstraint.constant > 0 {
            pickerBottomConstraint.constant = -94
        } else {
            pickerBottomConstraint.constant = 50
            rotate = 1*3.14
            self.pvStats.selectRow(Defaults[.selectedStats], inComponent: 0, animated: false)
        }
        
        UIView.animateWithDuration(0.2, animations: {
            self.view.layoutIfNeeded()
            self.ivStatsArraw.transform = CGAffineTransformMakeRotation(rotate)
            }, completion: { (value: Bool) in
            self.pvStats.selectRow(Defaults[.selectedStats], inComponent: 0, animated: false)
        })
    }
    
    @IBAction func actionChangeStatistic() {
        
        pickerBottomConstraint.constant = -94
        
        UIView.animateWithDuration(0.2) { () -> Void in
            self.view.layoutIfNeeded()
            self.ivStatsArraw.transform = CGAffineTransformMakeRotation(0)
        }
        
        Defaults[.selectedStats] = selectedStatsValue
        selectedStats = StatsTerms(rawValue: selectedStatsValue)!
        lblStats.text = selectedStats.description
        
        sellerStatistic(selectedStats) { (stats, error) -> () in
            if error == nil {
                self.stats = stats
            }
        }
    }
    
    // MARK: - Private
    
    func updateStats() {
        
        if let _ = stats?.views {
            lblViews.text = "\((stats?.views)!)"
            lblTurnover.text = stats?.turnover == nil ? "0" : stats?.turnover
            lblSell.text = stats?.sell == nil ? "0" : stats?.sell
            lblLikes.text = "\((stats?.likes)!)"
            lblFans.text = "\((stats?.fans)!)"
            lblRating.text = stats?.rating
            
            btnOrder.lblBadge?.text = "\((stats?.orders)!)"
            btnMail.lblBadge?.text = "\((stats?.mail)!)"
            
            NSNotificationCenter.defaultCenter().postNotificationName("notificationStats", object: nil, userInfo: ["orders": "\((stats?.orders)!)", "mail": "\((stats?.mail)!)"])
        }
        


    }
    
    func appEnterForeground(){
        if blurView.subviews.count > 2 {
            blurView.subviews[1].removeFromSuperview()
            blurView.subviews[1].removeFromSuperview()
        }
    }
    
    func loadData() {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
            
            let user = DataManager.manager.userRead()
            
            if user != nil {
                if let avatar = user?.avatar {
                    bottomViewPhone?.ivAvatar.image = UIImage(data: avatar)
                }
                bottomViewPhone?.lblName.text = user?.username
            }
        }
        
        sellerStatistic(selectedStats) { (stats, error) -> () in
            if error == nil {
                self.stats = stats
            }
        }
        
        dataListTypes{ (eventTypes, error) -> () in
            
            if error == nil {
                guard let eventTypes = eventTypes where eventTypes.count > 0 else {
                    return
                }
                self.eventTypes = eventTypes
                self.loadEvents()
            } else {
                super.networkError(error!)
            }
        }
    }
    
    private func loadEvents() {
        paging.next(fetchNextBatch, onFinish: updateDataSource)
    }
    
    func deviceDidRotate() {
        let currentOrientation = UIDevice.currentDevice().orientation
        
        // Ignore changes in device orientation if unknown, face up, or face down.
        if !UIDeviceOrientationIsValidInterfaceOrientation(currentOrientation) {
            return;
        }
        
        blurView.subviews.forEach({ $0.removeFromSuperview() })
        blurView.removeFromSuperview()
        
        //let isLandscape = UIDeviceOrientationIsLandscape(currentOrientation);
        let isPortrait = UIDeviceOrientationIsPortrait(currentOrientation);
        
        if isPortrait == true {
            btnMenu?.enabled = true
            btnMenu?.tintColor = nil
            
            let ivLogo = UIImageView.init(image: UIImage(named: "Logo"))
            self.navigationItem.titleView = ivLogo
            
        } else {
            btnMenu?.enabled = false
            btnMenu?.tintColor = UIColor.clearColor()
            
            self.navigationItem.titleView = nil
        }
    }
    
}


extension EventsViewController: UITableViewDataSource {
    
    // MARK: - Table datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithClass(EventsTableViewCell.self) as! EventsTableViewCell
        cell.layoutMargins = UIEdgeInsetsZero;
        
        let event = events[indexPath.row]
        
        cell.lblMessage.text = event.messagePlain
        cell.lblDate.text = event.date!.relativeTime//DateFormatters.dateListStyleFormatter.stringFromDate(event.date!)
        
        if let i = eventTypes.indexOf({$0.objId == event.idType}) {
            let et = eventTypes[i]
            
            cell.ivIcon.af_setImageWithURL(et.icoUrl!)
        }
        
        return cell
    }
    
}

extension EventsViewController: UITableViewDelegate {
    
    // MARK: - Table delegate
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == tableView.dataSource!.tableView(tableView, numberOfRowsInSection: indexPath.section) - 1 {
            paging.next(fetchNextBatch, onFinish: updateDataSource)
        }
    }
}

extension EventsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: - UIDataPicket delegate, datasource
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return StatsTerms.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return StatsTerms(rawValue: row)?.description
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedStatsValue = row
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 34
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let string = StatsTerms(rawValue: row)?.description
        return NSAttributedString(string: string!, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }
}

extension EventsViewController: UIPopoverPresentationControllerDelegate {
    
    // MARK: - Popover delegate
    
    func popoverPresentationController(popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverToRect rect: UnsafeMutablePointer<CGRect>, inView view: AutoreleasingUnsafeMutablePointer<UIView?>) {
        
        UIGraphicsBeginImageContextWithOptions(self.splitViewController!.view.bounds.size, true, 1)
        self.splitViewController!.view.drawViewHierarchyInRect(self.splitViewController!.view.bounds, afterScreenUpdates: true)
        
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let blur = screenshot.applyBlurWithRadius(3, tintColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.3), saturationDeltaFactor: 1.8, maskImage: nil)
        
        self.blurView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.blurView.frame = self.splitViewController!.view.bounds
        self.blurView.addSubview(UIImageView.init(image: blur))
        self.blurView.alpha = 1
        
        self.splitViewController!.view.insertSubview(self.blurView, atIndex: 100)
        
        let r:CGRect = self.sourceRectForCenteredPopoverController();
        rect.initialize(r);

    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        blurView.removeFromSuperview()
    }
    
}

extension EventsViewController {
    
    // MARK: - Paging
    
    private func fetchNextBatch(offset: Int, limit: Int, completion: (Array<Event>) -> Void) -> Void {
        
        userEvents(offset: offset, limit: paging.limit) { (events, error) -> () in
            if error == nil {
                
                guard let events = events where events.count > 0 else {
                    return
                }
                
                completion(events)
                
            } else {
                super.networkError(error!)
                //print((error?.code)!)
            }
        }
    }
    
    private func updateDataSource(items: Array<Event>) {
        self.events += items
    }
}
