//
//  Utils.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 20/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

public struct DateFormatters {
    public static var dateMediumStyleFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        return formatter
    }()
    
    public static var dateLongStyleFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.LongStyle
        
        return formatter
    }()
    
    public static var dateListStyleFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "d.M.YYYY HH:mm"
        
        return formatter
    }()
    
}


extension DefaultsKeys {
    
    static let firstRun = DefaultsKey<Bool>("firstRun")
    static let pushToken = DefaultsKey<String>("pushToken")
    static let selectedStats = DefaultsKey<Int>("selectedStats")
}
