//
//  LoginiPhoneViewController.swift
//  Pods
//
//  Created by Pavel Krusek on 24/01/16.
//
//

import UIKit

class LoginiPhoneViewController: LoginViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(15.0),
            NSForegroundColorAttributeName : UIColor.whiteColor(),
            NSUnderlineStyleAttributeName : NSUnderlineStyle.StyleSingle.rawValue
        ]
        
        let buttonTitleStr = NSMutableAttributedString(string:"Registrace", attributes:attrs)
        let attributedString = NSMutableAttributedString(string:"")
        attributedString.appendAttributedString(buttonTitleStr)
        btnRegister.setAttributedTitle(attributedString, forState: .Normal)
        
        btnLogin.backgroundColor = UIColor(red: 93.0/255.0, green: 176.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        btnLogin.layer.cornerRadius = 4
        
        tfName.attributedPlaceholder = NSAttributedString(string:tfName.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        tfPassword.attributedPlaceholder = NSAttributedString(string:tfPassword.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
            startObservingKeyboardEvents()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopObservingKeyboardEvents()
    }
    
    

    
    private func startObservingKeyboardEvents() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector:Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object:nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object:nil)
    }
    
    private func stopObservingKeyboardEvents() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size {
                let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
                scrollView.contentInset = contentInset
                scrollView.scrollIndicatorInsets = contentInset
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let contentInset = UIEdgeInsetsZero
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }


}

