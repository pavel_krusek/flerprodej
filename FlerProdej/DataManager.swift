//
//  DataManager.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 20/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import Foundation

class DataManager {
    
    static let manager = DataManager()
    
    private var userPath: NSURL?
    
    init() {
        let fileManager = NSFileManager.defaultManager()
        do {
            let documentDirectory = try fileManager.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false)
            userPath = documentDirectory.URLByAppendingPathComponent("user.dat")
        } catch {
            print(error)
        }
    }
    
    func userSave(user user: User) {
        
        guard let userPath = userPath else {
            return
        }
        let userData = NSKeyedArchiver.archivedDataWithRootObject(user)
        userData.writeToURL(userPath, atomically: true)
        
    }
    
    func userRead() -> User? {
        
        guard let userPath = userPath else {
            return nil
        }
        do {
            let userReadData = try NSData(contentsOfURL: userPath, options: .DataReadingMappedIfSafe)
            let user = NSKeyedUnarchiver.unarchiveObjectWithData(userReadData)
            return user as? User
            
        } catch {
            print(error)
            return nil
        }
    }

    
}