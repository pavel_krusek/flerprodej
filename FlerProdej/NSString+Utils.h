//
//  NSString+Utils.h
//  FlerProdej
//
//  Created by Pavel Krusek on 10/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

+ (NSString *)hmac:(NSString *)plainText withKey:(NSString *)key;

@end
