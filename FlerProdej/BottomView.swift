//
//  BottomView.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 17/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit

class BottomView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "BottomView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        view.frame = bounds

        self.addSubview(view);
    }

}
