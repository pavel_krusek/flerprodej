//
//  FlerAPI.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 09/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import Alamofire
import AlamofireImage
import Locksmith
import SwiftyUserDefaults
import JSONJoy
import UIKit

let notificationLogin = "notificationLogin"

var URL = "http://www.fler.cz/api/rest/"

func userLogin(name: String, password: String, callback: (Bool?, NSError?)->()) {
    
    var params = ["username": name, "pwd": password]
    
    if Defaults[.pushToken].characters.count > 0 {
        params["device_id"] = Defaults[.pushToken]
        params["device_platform"] = "ios"
    }
    
    let test: Request = Alamofire.request(.POST, "\(URL)user/auth", parameters: params).responseJSON { response in
    //Alamofire.request(.POST, "\(URL)user/auth", parameters: params, encoding:.JSON).responseJSON { response in
        switch response.result {
        case .Success:
            if let JSON = response.result.value {
                if let errorNumber = JSON["error_number"] {
                    if errorNumber != nil {
                        let error = NSError.init(domain: "", code: errorNumber as! Int, userInfo: nil)
                        callback(false, error)
                        return
                    }
                }
                let secretKey = JSON["secret_key"] as! String
                let sessionId = JSON["session_id"] as! String
                do {
                    let dictionary = Locksmith.loadDataForUserAccount("credentials")
                    if let _ = dictionary {
                        try Locksmith.updateData(["secretKey": secretKey, "sessionId": sessionId], forUserAccount: "credentials")
                    } else {
                        try Locksmith.saveData(["secretKey": secretKey, "sessionId": sessionId], forUserAccount: "credentials")
                    }
                } catch let error {
                    print(error)
                }
                let user = User(JSONDecoder(response.data!))
                Alamofire.request(.GET, user.avatarUrl!).responseImage { response in
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        user.avatarData = image
                    }
                    DataManager.manager.userSave(user: user)
                    NSNotificationCenter.defaultCenter().postNotificationName(notificationLogin, object: nil)
                    callback(true, nil)
                }
            }
        case .Failure(let error):
            callback(false, error)
        }
    }
}

func userSwitch(uid: Int, callback: (NSError?)->()) {
    
    let params = ["uid": uid]
    
    let headers = ["X-FLER-AUTHORIZATION": generateHash("POST", httpPath: "/api/rest/account/switch_account")]
    
    Alamofire.request(.POST, "\(URL)account/switch_account", parameters: params, headers: headers).responseJSON { response in
        
    }
    
}

func testError(uid: Int, callback: (NSError?)->()) {
    
    callback(NSError.init(domain: "", code: 0, userInfo: nil))
    
}

func userEvents(offset offset: Int, limit: Int, callback: (Array<Event>?, NSError?)->()) {
    
    let headers = ["X-FLER-AUTHORIZATION": generateHash("GET", httpPath: "/api/rest/user/events/list")]
    
    //let params = ["flow": "onme", "date_range": "2016-01-03:2016-01-10"]
    let params = ["flow": "onme", "offset": offset, "limit": limit]
    
    Alamofire.request(.GET, "\(URL)user/events/list", parameters: params as? [String : AnyObject], headers: headers).responseJSON { response in
        switch response.result {
        case .Success:
            if let data = response.data {
                if let events = Events(JSONDecoder(data)).events {
                    callback(events, nil)
                } else {
                    if let JSON = response.result.value {
                        if let errorNumber = JSON["error_number"] {
                            let error = NSError.init(domain: "", code: errorNumber as! Int, userInfo: nil)
                            callback(nil, error)
                        }
                    }
                }
            }
        case .Failure(let error):
            callback(nil, error)
        }
    }
}

func dataListTypes(callback: (Array<EventType>?, NSError?)->()) {
    
    let headers = ["X-FLER-AUTHORIZATION": generateHash("GET", httpPath: "/api/rest/user/events/datalist-types")]
    
    Alamofire.request(.GET, "\(URL)user/events/datalist-types", parameters: nil, headers: headers).responseJSON { response in
        switch response.result {
        case .Success:
            if let data = response.data {
                if let types = EventTypes(JSONDecoder(data)).eventTypes {
                    callback(types, nil)
                } else {
                    if let JSON = response.result.value {
                        if let errorNumber = JSON["error_number"] {
                            let error = NSError.init(domain: "", code: errorNumber as! Int, userInfo: nil)
                            callback(nil, error)
                        }
                    }
                }
            }
        case .Failure(let error):
            callback(nil, error)
        }
    }
}

func sellerStatistic(term: StatsTerms, callback: (Statistics?, NSError?)->()) {
    let headers = ["X-FLER-AUTHORIZATION": generateHash("GET", httpPath: "/api/rest/seller/statistics/overview")]
    
    //let params = ["term": term.apiValues]
    var params = [String: String]()
    
    if term.rawValue < 4 {
        params = ["term": term.apiValues]
    }
    else {
        params = ["term": "THIS_YEAR"]
    }
    Alamofire.request(.GET, "\(URL)seller/statistics/overview", parameters: params, headers: headers).responseJSON { response in
        switch response.result {
        case .Success:
            if let data = response.data {
                //if let stats = Statistics(JSONDecoder(data)) {
                    callback(Statistics(JSONDecoder(data)), nil)
//                } else {
//                    if let JSON = response.result.value {
//                        if let errorNumber = JSON["error_number"] {
//                            let error = NSError.init(domain: "", code: errorNumber as! Int, userInfo: nil)
//                            //callback(nil, error)
//                        }
//                    }
//                }
            }
        case .Failure(let error):
            callback(nil, error)
        }
    }
}

func generateHash(httpMethod: String, httpPath: String) -> String {
    
    let dictionary = Locksmith.loadDataForUserAccount("credentials")
    if let dictionary = dictionary {
        
        let secretKey = (dictionary["secretKey"])! as! String
        let sessionId = (dictionary["sessionId"])! as! String
        
        let timestamp = UInt64(NSDate().timeIntervalSince1970)
        
        let requestString = "\(httpMethod)\n" + "\(timestamp)\n" + "\(httpPath)"
        let requestStringHashed = NSString.hmac(requestString, withKey:secretKey).dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        let authString1 = "API1_SESS" + " " + sessionId + " "
        let authString = authString1 + "\(timestamp)" + " " + requestStringHashed
        
        return authString
    }

    return ""
    
}
