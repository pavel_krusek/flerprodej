//
//  Model.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 10/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import Foundation
import JSONJoy
import Locksmith


public enum StatsTerms : Int, CustomStringConvertible {
    case today = 0, week, month, year, all
    
    static var count: Int { return StatsTerms.all.rawValue + 1}
    
    public var description: String {
        switch self {
        case .today: return "Za poslední den";
        case .week: return "Za poslední týden";
        case .month: return "Za poslední měsíc";
        case .year: return "Za poslední rok";
        case .all: return "Vše";
        }
        
    }
    
    public var apiValues: String {
        switch self {
        case .today: return "TODAY";
        case .week: return "LAST_7_DAYS";
        case .month: return "LAST_30_DAYS";
        case .year: return "THIS_YEAR";
        case .all: return "";
        }
        
    }
}

//{
//    "secret_key": "33f4X3DBoZDkPuPDoG6IahMSx6DpdJdRjAH7tM1W",
//    "session_id": "Z1K07HWWvYgZAIc2l7Kc4ivFP39VIwn4E4WWPkvI",
//    "authmode": "pwd",
//    "expires": 1454269621,
//    "user": {
//        "uid": 402653,
//        "username": "rollingmobile",
//        "email": "itunes.fler@rollingmobile.cz",
//        "is_seller": "1",
//        "avatar_120x120": "http://img.flercdn.net/usr/avatar/4/0/402653/120_be2d53af16.jpg",
//        "avatar_default": 0,
//        "url_profile": "http://www.fler.cz/user/402653",
//        "pwd_hash": "p8qR9y92XBttR3q98nLjf7lw89eZ33bAYxzQ3wz9db3eORVlVLzH5Kl01sP7"
//    },
//    "connected_accounts": [{
//    "uid": 409954,
//    "username": "Vyvoj-test-prodej",
//    "avatar_120x120": "http://img.flercdn.net/usr/avatar/4/0/409954/120_f69d7cad9a.jpg",
//    "avatar_default": 0,
//    "url_shop": "http://www.fler.cz/shop/vyvoj-test-prodej",
//    "url_profile": "http://www.fler.cz/vyvoj-test-prodej",
//    "is_seller": "1"
//    }],
//    "seller": {
//        "url_shop": "http://www.fler.cz/shop/rollingmobile",
//        "fler_rank": "36.47"
//    }
//}

// MARK: - User

class User: NSObject, JSONJoy, NSCoding {
    
    var objId: Int?
    var username: String?
    var email: String?
    var avatarUrl: NSURL?
    var avatar: NSData?
    var avatarData: UIImage?
    var flerRank: String?
    
    var connectedAccounts: Array<User>?
    
    required init(_ decoder: JSONDecoder) {
        
        if let _ = decoder["user"].dictionary {
            objId = decoder["user"]["uid"].integer
            username = decoder["user"]["username"].string
            email = decoder["user"]["email"].string
            avatarUrl = decoder.urlFromString(decoder["user"]["avatar_120x120"].string)
        } else {
            objId = decoder["uid"].integer
            username = decoder["username"].string
            avatarUrl = decoder.urlFromString(decoder["avatar_120x120"].string)
        }
        
        flerRank = decoder["seller"]["fler_rank"].string
        
        if let data = decoder["connected_accounts"].array {
            connectedAccounts = Array<User>()
            for decoder in data {
                connectedAccounts!.append(User(decoder))
            }
        }
    }
    
    init(objId: Int, username: String, email: String, avatar: NSData?, avatarUrl: NSURL?, flerRank: String?, connectedAccounts: Array<User>?) {
        self.objId = objId
        self.username = username
        self.email = email
        self.avatar = avatar
        self.avatarUrl = avatarUrl
        self.flerRank = flerRank
        self.connectedAccounts = connectedAccounts
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        //guard let name = aDecoder.decodeObjectForKey("username") as? String, let email = aDecoder.decodeObjectForKey("email") as? String else { return nil }
        guard let name = aDecoder.decodeObjectForKey("username") as? String else { return nil }
        
        if let accounts = aDecoder.decodeObjectForKey("connectedAccounts") as? Array<User> {
            self.init(objId:aDecoder.decodeIntegerForKey("objId"), username: name, email: "", avatar: aDecoder.decodeObjectForKey("avatar") as? NSData, avatarUrl: aDecoder.decodeObjectForKey("avatarUrl") as? NSURL, flerRank: aDecoder.decodeObjectForKey("flerRank") as? String, connectedAccounts: accounts)
        } else {
            self.init(objId:aDecoder.decodeIntegerForKey("objId"), username: name, email: "", avatar: aDecoder.decodeObjectForKey("avatar") as? NSData, avatarUrl: aDecoder.decodeObjectForKey("avatarUrl") as? NSURL, flerRank: aDecoder.decodeObjectForKey("flerRank") as? String, connectedAccounts: nil)
        }
        
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        if let avatarData = avatarData {
            avatar = UIImageJPEGRepresentation(avatarData, 0.9);
            aCoder.encodeObject(avatar, forKey:"avatar");
        }
        if let connectedAccounts = connectedAccounts {
            aCoder.encodeObject(connectedAccounts, forKey:"connectedAccounts");
        }
        aCoder.encodeObject(email, forKey: "email")
        aCoder.encodeObject(username, forKey: "username")
        aCoder.encodeInteger(objId!, forKey: "objId")
        aCoder.encodeObject(avatarUrl, forKey: "avatarUrl")
        aCoder.encodeObject(flerRank, forKey: "flerRank")
    }
    
    static func isLogged() -> Bool {
        
        let dictionary = Locksmith.loadDataForUserAccount("credentials")
        if let _ = dictionary {
            return true
        }
        return false
    }
    
}


// MARK: - Event

//"id": 310862135,
//"timestamp": 1452104807,
//"date": "2016-01-06 19:26:47",
//"id_type": 36,
//"id_group": 6,
//"uid_author": 9413,
//"message_html": "Uživateli <a href=\"http://www.fler.cz/programator\">programator</a> se líbí moje zboží <a href=\"http://www.fler.cz/zbozi/testovaci-zbozi-6994494\">Testovací zboží</a>.",
//"message_plain": "Uživateli programator se líbí moje zboží Testovací zboží."

//date - neni time zone, timestamp?

struct Event: JSONJoy {
    
    var objId: Int?
    var messageHtml: String?
    var messagePlain: String?
    var date: NSDate?
    var idType: Int?
    
    init(_ decoder: JSONDecoder) {
        
        objId = decoder["id"].integer
        messageHtml = decoder["message_html"].string
        messagePlain = decoder["message_plain"].string
        date = NSDate(timeIntervalSince1970: decoder["timestamp"].double!)
        idType = decoder["id_type"].integer
    }
    
//    var icon: NSURL {
//        get {
//            
//        }
//    }
}

struct Events : JSONJoy {
    
    var events: Array<Event>?
    
    init(_ decoder: JSONDecoder) {
        if let data = decoder.array {
            events = Array<Event>()
            for decoder in data {
                events!.append(Event(decoder))
            }
        }
    }
}

struct EventType: JSONJoy {
    
    var objId: Int?
    var icoUrl: NSURL?
    
    init(_ decoder: JSONDecoder) {
        objId = decoder["id"].integer
        icoUrl = decoder.urlFromString(decoder["ico_100"].string)
    }
}

struct EventTypes : JSONJoy {
    
    var eventTypes: Array<EventType>?
    
    init(_ decoder: JSONDecoder) {
        if let data = decoder.array {
            eventTypes = Array<EventType>()
            for decoder in data {
                eventTypes!.append(EventType(decoder))
            }
        }
    }
}

struct Statistics: JSONJoy {
    var views: Int?
    var turnover: String?
    var sell: String?
    var likes: Int?
    var fans: Int?
    var rating: String?
    var orders: Int?
    var mail: Int?
    
    init(_ decoder: JSONDecoder) {
        
        views = decoder["views"]["total"].integer
        turnover = decoder["turnover"]["total"].string
        sell = decoder["turnover"]["total"].string
        likes = decoder["likes"]["count"].integer
        fans = decoder["fans"]["count"].integer
        rating = decoder["sellrating"]["count"].string
        orders = decoder["orders"]["count.new"].integer
        mail = decoder["flerpost"]["message.count.new"].integer
    }
}

//navstevy - views.total
//obrat - turnover.total
//prodej - turnover.total
//lajky - likes.count
//nadsenci - json.fans
//hodnoceni - sellrating.count
//
//objednavky - orders.count.new
//mail = flerpost.message.count.new
//
//udalosti - ???

//{
//    "views": {
//        "total": 0,
//        "shop": 0,
//        "shop.sold": 0,
//        "profile": 0
//    },
//    "likes": {
//        "count": 0
//    },
//    "turnover": {
//        "total": 0,
//        "currency": "CZK"
//    },
//    "sellrating": {
//        "average": null,
//        "count": "0"
//    },
//    "fans": {
//        "count": 0
//    },
//    "orders": {
//        "count.total": 0,
//        "count.new": 1
//    },
//    "flerpost": {
//        "message.count.new": 2
//    }
//}


// MARK: - Extensions

extension JSONDecoder {
    
    func urlFromString(str: String?) -> NSURL? {
        if let str = str {
            return NSURL(string: str)
        }
        return nil
    }


    func dateFromTimestamp(timestamp: Double?) -> NSDate? {
        FIXME() //overit nil pro formatter
        if let timestamp = timestamp {
            return NSDate(timeIntervalSince1970: timestamp)
        }
        return nil
    }

}
