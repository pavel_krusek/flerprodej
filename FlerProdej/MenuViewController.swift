//
//  MasterViewController.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 09/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit
import LGSideMenuController

class MenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vAvatar: UIView!
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblSettings: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var vTop: UIView!
    @IBOutlet weak var cTopHeight: NSLayoutConstraint!
    @IBOutlet weak var cDividerLeading: NSLayoutConstraint!
    @IBOutlet weak var cDividerTrailing: NSLayoutConstraint!
    @IBOutlet weak var cLogoTop: NSLayoutConstraint!
    @IBOutlet weak var cUsernameCenter: NSLayoutConstraint!

    private var menuItems = [MenuItem]()
    
    var orders: String = ""
    var mail: String = ""
    
    // MARK: - LifeCycle
    
    deinit {
        print("deinit \(self)")
        //NSNotificationCenter.defaultCenter().removeObserver(self, name: "NotificationIdentifier", object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
            vTop.hidden = true
            cTopHeight.constant = 65
            cDividerLeading.constant = 15
            cDividerTrailing.constant = 15
            cUsernameCenter.constant = -10
        } else {
//            if UIDevice.currentDevice().orientation.isLandscape.boolValue {
//                cLogoTop.constant = 0
//            } else {
//                cLogoTop.constant = -64
//            }
        }
        
        
        menuItems = [MenuItem(title: "Objednávky", target: "", isBadge: true), MenuItem(title: "Moje zboží", target: "", isBadge: false), MenuItem(title: "Pošta", target: "", isBadge: true), MenuItem(title: "Události", target: "", isBadge: true), MenuItem(title: "Profil", target: "", isBadge: false)]
        
        self.view.backgroundColor = UIColor(red: 152.0/255.0, green: 205.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        
        vAvatar.backgroundColor = UIColor(red: 93.0/255.0, green: 176.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        vAvatar.layer.cornerRadius = vAvatar.frame.size.height / 2.0;
        
        lblSettings.textColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        lblUsername.textColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        lblRating.textColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
        ivAvatar.layer.cornerRadius = ivAvatar.frame.size.height / 2.0
        ivAvatar.clipsToBounds = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "notificateLogin", name: notificationLogin, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "notificationStats:", name: "notificationStats", object: nil)
        
        notificateLogin()
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "notificationStats:", name: "notificationStats", object: nil)

    }

    @objc private func notificateLogin() {
        let user = DataManager.manager.userRead()
        
        if user != nil {
            if let avatar = user?.avatar {
                ivAvatar.image = UIImage(data: avatar)
            }
            lblUsername.text = user?.username
            
            if let flerRank = user?.flerRank {
                lblRating.text = "\(flerRank)/100"
            }
            
        }
    }
    
    func notificationStats(notification: NSNotification) {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        orders = userInfo["orders"]!
        mail = userInfo["mail"]!
        
        tableView.reloadData()
    }
    
    @IBAction func actionLogout() {
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
            //let vc = self.storyboard?.instantiateViewControllerWithIdentifier((String.className(LoginiPhoneViewController)))
//            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            let mainController = appDelegate.window?.rootViewController as! LGSideMenuController
//            let nvc = mainController.rootViewController as! UINavigationController
//            let vc = nvc.topViewController as! EventsViewController
            
            let nvc = self.slidingPanelController.centerViewController as! UINavigationController
            let vc = nvc.topViewController as! EventsViewController
            vc.switchUser()
            //mainController.presentViewController(vc!, animated: true, completion: nil)
        } else {
//            let vc = self.storyboard?.instantiateViewControllerWithIdentifier((String.className(LoginViewController)))
//            self.presentViewController(vc!, animated: true, completion: nil)
            let navigationController = self.splitViewController!.viewControllers[self.splitViewController!.viewControllers.count-1] as! UINavigationController
            let vc = navigationController.topViewController as! EventsViewController
            vc.switchUser()
        }
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.currentDevice().orientation.isLandscape.boolValue {
            cLogoTop.constant = 0
        } else {
            cLogoTop.constant = -64
        }
    }


}

extension MenuViewController: UITableViewDataSource {
    
    // MARK: - Table View Data Source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithClass(MenuTableViewCell.self) as! MenuTableViewCell
        
        let menuItem = menuItems[indexPath.row]
        
        cell.lblTitle.text = menuItem.title
        
        if menuItem.isBadge == true {
            cell.vBadge.hidden = false
        } else {
            cell.vBadge.hidden = true
        }
        
        if indexPath.row == menuItems.count - 1 {
            cell.vDivider.hidden = true
        } else {
            cell.vDivider.hidden = false
        }
        
        if indexPath.row == 0 {
            cell.lblBadge.text = orders
        }
        
        if indexPath.row == 2 {
            cell.lblBadge.text = mail
        }
        
        return cell
    }


}

struct MenuItem {
    var title: String
    var target: String
    var isBadge: Bool
}

