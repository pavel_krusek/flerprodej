//
//  EventsTableViewCell.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 17/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        ivIcon.image = nil
        ivIcon.af_cancelImageRequest()
        lblMessage.text = ""
        //lblDate.text = ""
    }

}
