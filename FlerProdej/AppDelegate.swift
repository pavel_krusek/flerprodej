//
//  AppDelegate.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 09/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit
import SwiftLoader
import LGSideMenuController
import SwiftyUserDefaults
import Locksmith



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    var splashWindow: UIWindow?
    var isSplash = false
    
    var rearController: MenuViewController?

    // MARK: - Application Lifecycle
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        if Defaults[.firstRun] == false {
            Defaults[.firstRun] = true
            Defaults[.selectedStats] = 2
            do {
                let dictionary = Locksmith.loadDataForUserAccount("credentials")
                if let _ = dictionary {
                    try Locksmith.deleteDataForUserAccount("credentials")
                }
            } catch let error {
                print(error)
            }
        }
        
//        let lagFreeField: UITextField = UITextField()
//        self.window?.addSubview(lagFreeField)
//        lagFreeField.becomeFirstResponder()
//        lagFreeField.resignFirstResponder()
//        lagFreeField.removeFromSuperview()
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            
            let splitViewController = self.window!.rootViewController as! UISplitViewController
            //        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
            //        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
            //        splitViewController.delegate = self
            
            splitViewController.view.backgroundColor = UIColor(red: 152.0/255.0, green: 205.0/255.0, blue: 203.0/255.0, alpha: 1.0)
            splitViewController.maximumPrimaryColumnWidth = 235
            
            splitViewController.preferredDisplayMode = .Automatic
        } else {
            self.window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
//            
//            rearController = storyboard.instantiateViewControllerWithIdentifier(String.className(MenuViewController)) as? MenuViewController
//            let mainController = storyboard.instantiateViewControllerWithIdentifier("EventsController")
//            
//            let revealController = LGSideMenuController(rootViewController: mainController)
//            revealController.setLeftViewEnabledWithWidth(158, presentationStyle: LGSideMenuPresentationStyle.SlideBelow, alwaysVisibleOptions: LGSideMenuAlwaysVisibleOptions.OnNone)
//            //rearController?.view.frame = CGRectMake(0, 0, 158, (rearController?.view.frame.size.height)!)
//            revealController.leftView().addSubview(rearController!.view)
//            revealController.leftViewInititialOffsetX = 0
//            revealController.rootViewLayerShadowRadius = 0
//            revealController.leftViewCoverColor = .clearColor()
//            revealController.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOptions.OnAll
//            revealController.leftViewStatusBarStyle = UIStatusBarStyle.LightContent
//            
//            if let window = window {
//                window.rootViewController = revealController
//                window.makeKeyAndVisible()
//            }
            
            let menuController = storyboard.instantiateViewControllerWithIdentifier(String.className(MenuViewController)) as? MenuViewController
            let mainController = storyboard.instantiateViewControllerWithIdentifier("EventsController")
            
            let slidingController = MSSlidingPanelController.init(centerViewController: mainController, andLeftPanelController: menuController)
            slidingController.setLeftPanelMaximumWidth(UIScreen.mainScreen().bounds.size.width/2, withCompletion: { () -> Void in
                
            })
            
            if let window = window {
                window.rootViewController = slidingController
                window.makeKeyAndVisible()
            }


        }

        
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()

        appearanceProxy()
        
        //printFonts()
        
        if User.isLogged() == false {
            isSplash = true
            
            let loginController: UIViewController?
            
            if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
                loginController = storyboard.instantiateViewControllerWithIdentifier(String.className(LoginViewController))
            } else {
                loginController = storyboard.instantiateViewControllerWithIdentifier(String.className(LoginiPhoneViewController))
            }
            
            splashWindow = UIWindow.init(frame: UIScreen.mainScreen().bounds)
            splashWindow!.windowLevel = UIWindowLevelNormal + 1;
            splashWindow?.rootViewController = loginController
            splashWindow?.makeKeyAndVisible()
        }
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
    }
    
    func applicationWillTerminate(application: UIApplication) {
    }

    
    func dissmissSplashWindow() {
        
        UIView.animateWithDuration(0.6, delay: 0.0, options: .CurveEaseOut, animations: {
            self.splashWindow?.alpha = 0
            
            }, completion: { finished in
                self.splashWindow = nil
                
        })
    }
    
    // MARK: - Interface orientations
    
    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            return UIInterfaceOrientationMask.All
        }
        
        return UIInterfaceOrientationMask.Portrait
    }
    
    // MARK: - Remote notifications
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("")
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let tokenAsString = deviceToken.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
        let cleanToken = tokenAsString.stringByReplacingOccurrencesOfString(" ", withString: "")
        
        Defaults[.pushToken] = cleanToken
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError){
        print(error)
    }

    // MARK: - Split view

//    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
////        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
////        guard let topAsDetailController = secondaryAsNavController.topViewController as? EventsViewController else { return false }
////        if topAsDetailController.detailItem == nil {
////            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
////            return true
////        }
//        return false
//    }
    
    // MARK: - Appearance
    
    private func appearanceProxy() {
        
        let navigationBarAppearace = UINavigationBar.appearance()
        
        navigationBarAppearace.tintColor = UIColor.whiteColor()
        navigationBarAppearace.barTintColor = UIColor.whiteColor()//UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 194.0/255.0, alpha: 1.0)
        //hrd9@fb006
        
        let font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        if let font = font {
            navigationBarAppearace.titleTextAttributes = [NSFontAttributeName : font, NSForegroundColorAttributeName : UIColor.whiteColor()]
        }
        
        navigationBarAppearace.setBackgroundImage(UIImage.imageWithColor(UIColor(red: 93.0/255.0, green: 176.0/255.0, blue: 174.0/255.0, alpha: 1.0)), forBarPosition: .Any, barMetrics: .Default)
        navigationBarAppearace.shadowImage = UIImage()
        
        //        //navigationBarAppearace.translucent = true
        //        //navigationBarAppearace.barStyle = UIBarStyle.Black
        //
        //        let barButton = UIBarButtonItem.appearance()
        //
        //        if let font = UIFont(name: "HelveticaNeue", size: 12) {
        //            barButton.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName : UIColor.whiteColor()], forState: UIControlState.Normal)
        //        }
        //
        //        let navigationBarMenuAppearace = UINavigationBar.swiftAppearanceWhenContainedIn(MenuNavigationController.self)
        //
        //        navigationBarMenuAppearace.setBackgroundImage(UIImage(), forBarPosition: .Any, barMetrics: .Default)
        //        //navigationBarMenuAppearace.shadowImage = UIImage.imageWithColor(UIColor.whiteColor())
        //        navigationBarMenuAppearace.tintColor = UIColor.whiteColor()
        
        //[[UIPickerView appearance] setBackgroundColor:[UIColor whiteColor];
        //UIPickerView.appearance().backgroundColor = .whiteColor()
        
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 150
        config.spinnerColor = .redColor()
        config.backgroundColor = UIColor(red: 93.0/255.0, green: 176.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        //config.foregroundColor = .blackColor()
        //config.foregroundAlpha = 0.5
        
        SwiftLoader.setConfig(config)
    }
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames()
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNamesForFamilyName(familyName)
            print("Font Names = [\(names)]")
        }
    }


}

