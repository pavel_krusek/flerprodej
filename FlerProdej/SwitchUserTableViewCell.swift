//
//  SwitchUserTableViewCell.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 31/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit

class SwitchUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var vBg: UIView!
    @IBOutlet weak var vBgIcon: UIView!
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .clearColor()
        vBg.backgroundColor = UIColor(red: 173.0/255.0, green: 215.0/255.0, blue: 213.0/255.0, alpha: 1.0)
        vBgIcon.backgroundColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
        vBgIcon.layer.cornerRadius = 2
        
        lblName.textColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        ivIcon.image = nil
        ivIcon.af_cancelImageRequest()
        lblName.text = ""
    }

}
