//
//  SwitchUserViewController.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 31/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit

class SwitchUserViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblStaticHeading: UILabel!
    
    var accounts = Array<User>()
    
    var dismissCallback: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 152.0/255.0, green: 205.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        
        tableView.backgroundColor = .clearColor()
        tableView.estimatedRowHeight = 74.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        btnCancel.backgroundColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        btnCancel.layer.cornerRadius = 4
        
        lblStaticHeading.textColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let view = self.view.superview {
            view.layer.cornerRadius = 10
        }
    }
    

    @IBAction func actionCancel() {
        self.dismissViewControllerAnimated(true, completion: nil)
        if let dismissCallback = self.dismissCallback {
            dismissCallback()
        }
    }

}

extension SwitchUserViewController: UITableViewDataSource {
    
    // MARK: - Table datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accounts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithClass(SwitchUserTableViewCell.self) as! SwitchUserTableViewCell
        
        let user = accounts[indexPath.row]
        
        cell.lblName.text = user.username
        if let url = user.avatarUrl {
            cell.ivIcon.af_setImageWithURL(url)
        }

        return cell
    }
    
}

extension SwitchUserViewController: UITableViewDelegate {
    
    // MARK: - Table delegate
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let user = accounts[indexPath.row]
        
        userSwitch(user.objId!) { (error) -> () in
            
        }

    }
}
