//
//  MenuTableViewCell.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 20/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vBadge: UIView!
    @IBOutlet weak var lblBadge: UILabel!
    @IBOutlet weak var vDivider: UIView!
    
    @IBOutlet weak var cLeading: NSLayoutConstraint!
    @IBOutlet weak var cTrailing: NSLayoutConstraint!
    @IBOutlet weak var cBadge: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

        
        self.backgroundColor = UIColor.clearColor()
        
        lblTitle.textColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        lblBadge.textColor = UIColor(red: 32.0/255.0, green: 109.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
        vBadge.backgroundColor = UIColor.clearColor()
        vBadge.layer.borderWidth = 1;
        vBadge.layer.borderColor = UIColor(red: 93.0/255.0, green: 176.0/255.0, blue: 174.0/255.0, alpha: 1.0).CGColor
        vBadge.layer.cornerRadius = vBadge.frame.size.height / 2.0


        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
            cLeading.constant = 15
            cTrailing.constant = 15
            cBadge.constant = 8
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        lblTitle.text = ""
    }

}
