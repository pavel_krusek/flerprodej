//
//  BaseViewController.swift
//  FlerProdej
//
//  Created by Pavel Krusek on 10/01/16.
//  Copyright © 2016 Fler. All rights reserved.
//

import UIKit
import LGSideMenuController

class BaseViewController: UIViewController {
    
    var bottomViewPhone: BottomViewPhone?
    
    deinit {
        print("deinit \(self)")
    }
    
    func addBottomView() {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            let bottomView = BottomView.init(frame: CGRectZero)
            bottomView.translatesAutoresizingMaskIntoConstraints = false
            
            self.view.addSubview(bottomView)
            
            let views = ["bottomView": bottomView]
            
            var allConstraints = [NSLayoutConstraint]()
            
            let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[bottomView(50)]-0-|", options: [], metrics: nil, views: views)
            allConstraints += verticalConstraints
            
            let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[bottomView]-0-|", options: [], metrics: nil, views: views)
            allConstraints += horizontalConstraints
            
            NSLayoutConstraint.activateConstraints(allConstraints)
        } else {
            let bottomView = BottomViewPhone.init(frame: CGRectZero)
            bottomView.translatesAutoresizingMaskIntoConstraints = false
            
            self.view.addSubview(bottomView)
            
            let views = ["bottomView": bottomView]
            
            var allConstraints = [NSLayoutConstraint]()
            
            let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[bottomView(50)]-0-|", options: [], metrics: nil, views: views)
            allConstraints += verticalConstraints
            
            let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[bottomView]-0-|", options: [], metrics: nil, views: views)
            allConstraints += horizontalConstraints
            
            NSLayoutConstraint.activateConstraints(allConstraints)
            
            bottomView.switchCallback = { [weak self] in
                self?.switchUser()
            }
            
            bottomViewPhone = bottomView
        }

        
    }
    
    func switchUser() {
        
    }
    
    func networkError(error: NSError) {
        var text = ""
        
        if error.code == -1009 {
            text = "Offline"
            let alert = UIAlertController(title: "", message:text, preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default) { _ in
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        //} else if error.code == 602 {
        } else {
            let loginController: UIViewController?
            
            if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
                loginController = self.storyboard!.instantiateViewControllerWithIdentifier(String.className(LoginViewController))
            } else {
                loginController = self.storyboard!.instantiateViewControllerWithIdentifier(String.className(LoginiPhoneViewController))
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let mainController = appDelegate.window?.rootViewController as! LGSideMenuController
                mainController.presentViewController(loginController!, animated: true, completion: nil)
                return
            }
            
            self.presentViewController(loginController!, animated: true, completion: nil)
            return
        }
//        else {
//            text = "Network error"
//        }
        
        let alert = UIAlertController(title: "", message:text, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default) { _ in
        }
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }


}
